/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptionhandling;

import java.util.Random;

/**
 *
 * @author soyae
 */
public class Healer extends Character {

    private final int defense = 10;
    private final int attack = 10;
    private int HP = 70;
    private final int hitPercentage = 85;
    private int count = 1;

    public Healer(String name) {
        super(name);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean attack() {
        
        if(getCount() > 1) {
            heal();
        } else {
            setCount(getCount() + 1);
        }
        
        Random r = new Random();
        int ramdomNumber = r.nextInt(100);
        //random any number from 0-99
        return ramdomNumber < getHitPercentage();
    }

    /**
     *
     * @param damage
     */
    @Override
    public void receiveDamage(int damage) {
        final int trueDamage = damage - getDefense();
        final int latestHP = getHP() - trueDamage;
        
        if ((latestHP) >= 0) {
            setHP(latestHP);
        } else {
            setHP(0);
        }
        
    }

    /**
     *
     */
    @Override
    public void info() {
        String className = this.getClass().getSimpleName();

        System.out.println("------------STATUS------------");
        System.out.println("Role        : " + className);
        System.out.println("HP          : " + getHP());
        System.out.println("Attack      : " + getAttack());
        System.out.println("Defense     : " + getDefense());

    }
    
    public void heal() {
        final int afterHeal = (getHP() + 25);
        System.out.println("Menggunakan skill heal");
        setHP(afterHeal);
        setCount(1);
    }

    public int getDefense() {
        return defense;
    }

    public int getAttack() {
        return attack;
    }

    public int getHP() {
        return HP;
    }

    private void setHP(int HP) {
        this.HP = HP;
    }

    public int getHitPercentage() {
        return hitPercentage;
    }
    
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
