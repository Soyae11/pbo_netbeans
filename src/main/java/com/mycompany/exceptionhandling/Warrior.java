/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptionhandling;

import java.util.Random;

/**
 *
 * @author soyae
 */
public class Warrior extends Character {

    private final int defense = 30;
    private final int attack = 25;
    private int HP = 80;
    private final int hitPercentage = 60;

    public Warrior(String name) {
        super(name);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean attack() {
        Random r = new Random();
        int ramdomNumber = r.nextInt(100);
        //random any number from 0-99
        return ramdomNumber < getHitPercentage();
    }

    /**
     *
     * @param damage
     */
    @Override
    public void receiveDamage(int damage) {
        final int trueDamage = damage - getDefense();
        final int latestHP = getHP() - trueDamage;

        if ((latestHP) >= 0) {
            setHP(latestHP);
        } else {
            setHP(0);
        }
    }

    /**
     *
     */
    @Override
    public void info() {
        String className = this.getClass().getSimpleName();

        System.out.println("------------STATUS------------");
        System.out.println("Role        : " + className);
        System.out.println("HP          : " + getHP());
        System.out.println("Attack      : " + getAttack());
        System.out.println("Defense     : " + getDefense());

    }

    public int getDefense() {
        return defense;
    }

    public int getAttack() {
        return attack;
    }

    public int getHP() {
        return HP;
    }

    private void setHP(int HP) {
        this.HP = HP;
    }

    public int getHitPercentage() {
        return hitPercentage;
    }
}
