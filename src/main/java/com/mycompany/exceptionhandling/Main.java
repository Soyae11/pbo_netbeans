/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptionhandling;

import java.util.Scanner;

/**
 *
 * @author soyae
 */
public class Main {

    private String playerName;
    private int characterNumber;
    private final String[] characterRoleList = {"Magician", "Healer", "Warrior"};
    Character character, titan;

    public static void main(String[] args) {

        Main main = new Main();
        System.out.println("Selamat datang di game Defend FILDOM!");

        main.inputName();
    }

    public void inputName() {

        System.out.println("Silahkan masukkan nama player : ");

        Scanner input = new Scanner(System.in);
        this.playerName = input.nextLine();

        inputCharacter();
    }

    public void inputCharacter() {

        System.out.println("Silahkan pilih karakter yang anda inginkan : ");
        for (int i = 0; i < characterRoleList.length; i++) {
            System.out.println((i + 1) + ". " + characterRoleList[i]);
        }

        Scanner input = new Scanner(System.in);
        if (input.hasNextInt()) {
            characterNumber = input.nextInt();
            defineCharacter();
        } else {
            System.out.println("Tolong masukkan angka!");
            input.next();
            inputCharacter();
        }
    }

    public void defineCharacter() {
        titan = new Titan();
        switch (characterNumber) {
            case 1:
                character = new Magician(this.playerName);
                introduction();
                break;
            case 2:
                character = new Healer(this.playerName);
                introduction();
                break;
            case 3:
                character = new Warrior(this.playerName);
                introduction();
                break;
            default:
                System.out.println("Karakter yang anda pilih tidak ditemukan!");
                inputCharacter();
                break;
        }
    }

    public void introduction() {
        int count = 1;
        System.out.println("Selamat datang, " + character.getName() + "!");
        character.info();
        while (character.getHP() > 0 && titan.getHP() > 0) {
            System.out.println("============ TURN " + count + " ============");
            battle();
            System.out.println("Enemy's HP  : " + titan.getHP());
            System.out.println( character.getName() + "'s HP  : " + character.getHP());
            count++;
        }
        aftermath();
    }

    public void battle() {
        if (character.attack() && character.getHP() != 0) {
            titan.receiveDamage(character.getAttack());
        }
        
        if (titan.attack() && titan.getHP() != 0) {
            character.receiveDamage(titan.getAttack());
        }
    }
    
    public void aftermath() {
        System.out.println("========================");
        
        if(character.getHP() < 1) {
            System.out.println(titan.getName() + " menang");
        } else {
            System.out.println(character.getName() + " menang");
        }
        
        System.out.println("============ PLAYER ============");
        character.info();
        
        System.out.println("============ ENEMY ============");
        titan.info();
    }
}
