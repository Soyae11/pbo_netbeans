/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptionhandling;

import java.util.Random;

/**
 *
 * @author soyae
 */
public class Titan extends Character {

    private int defense = 0;
    private int attack = 45;
    private int HP = 200;
    private int hitPercentage = 40;

    public Titan() {
        super();
    }

    public boolean attack() {
        Random r = new Random();
        int ramdomNumber = r.nextInt(100);
        //random any number from 0-99
        return ramdomNumber < getHitPercentage();
    }

    public void receiveDamage(int damage) {
        final int trueDamage = damage - getDefense();
        final int latestHP = getHP() - trueDamage;

        if ((latestHP) >= 0) {
            setHP(latestHP);
        } else {
            setHP(0);
        }
    }

    public void info() {
        String className = this.getClass().getSimpleName();

        System.out.println("------------STATUS------------");
        System.out.println("Role        : " + className);
        System.out.println("HP          : " + getHP());
        System.out.println("Attack      : " + getAttack());
        System.out.println("Defense     : " + getDefense());

    }

    public int getDefense() {
        return defense;
    }

    public int getAttack() {
        return attack;
    }

    public int getHP() {
        return HP;
    }

    private void setHP(int HP) {
        this.HP = HP;
    }

    public int getHitPercentage() {
        return hitPercentage;
    }
}
