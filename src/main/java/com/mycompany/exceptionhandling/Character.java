/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exceptionhandling;

import java.util.Random;

/**
 *
 * @author soyae
 */
abstract class Character {
    
    private String name;
    
    public Character(String name){
        this.name = name;
    }
    public Character(){
        this.name = "Titan";
    }

    public abstract boolean attack();

    public abstract void receiveDamage(int damage);

    public abstract void info();
    
    public abstract int getDefense();

    public abstract int getAttack();

    public abstract int getHP();

    public abstract int getHitPercentage();

    public String getName() {
        return name;
    }
}
